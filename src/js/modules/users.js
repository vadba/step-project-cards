import { User } from '../classes/User.js';
import { List } from '../classes/List.js';

import { Visit } from '../classes/Visit.js';
import { VisitDentist } from '../classes/VisitDentist.js';
import { VisitCardiologist } from '../classes/VisitCardiologist.js';
import { VisitTherapist } from '../classes/VisitTherapist.js';

import { request } from '../tools/fetch.js';

const registWrap = document.querySelector('.regist-wrap');
const logintWrap = document.querySelector('.login-wrap');
const authWrap = document.getElementById('modal');
const loginLink = document.querySelector('.login-link');
const registerLink = document.querySelector('.register-link');

const openModalButtons = document.querySelector('[data-modal-target]');
const closeModalButtons = document.querySelector('[data-close-button]');
const overlay = document.getElementById('overlay');

const loginForm = document.getElementById('login');
const err = document.querySelector('.error-login');

const buttonCreateVisit = document.querySelector('.button-test');
const buttonLogout = document.querySelector('.logout');
const buttonLogin = document.querySelector('.login');

const noContent = document.querySelector('.no-content');

const navFilter = document.querySelector('.nav-filter');
const list = new List('/cards');

openModalButtons.addEventListener('click', () => {
    err.textContent = '';
    openModal(authWrap);
});

loginForm.addEventListener('submit', e => {
    e.preventDefault();
    const form = new FormData(e.target);
    const email = form.get('email');
    const password = form.get('password');
    const user = new User(email, password);
    login(user);
});

overlay.addEventListener('click', () => {
    closeModal(authWrap);
});

closeModalButtons.addEventListener('click', () => {
    closeModal(authWrap);
});

function openModal(modal) {
    if (modal == null) return;
    modal.classList.add('active');
    overlay.classList.add('active');
}

function closeModal(modal) {
    if (modal == null) return;
    modal.classList.remove('active');
    overlay.classList.remove('active');
}

buttonLogout.addEventListener('click', () => {
    const user = new User();
    user.logout();
    checkUserAuth();
});

authWrap.addEventListener('click', e => {
    if (e.target === loginLink) {
        logintWrap.classList.toggle('show');
        registWrap.classList.toggle('show');
    } else if (e.target === registerLink) {
        registWrap.classList.toggle('show');
        logintWrap.classList.toggle('show');
    }
});

const vissit = new Visit('/cards');

vissit.createForm();

vissit.createVisit(list);


function checkUserAuth() {
    const user = new User();
    if (user.getToken()) {
        navFilter.classList.remove('no-filter-content-hidden');
        buttonCreateVisit.classList.add('show');
        buttonLogout.classList.add('show');
        buttonLogin.classList.remove('show');
        noContent.classList.add('no-content-hidden');

        list.displayList().then(items => {
            vissit.editVisit(items);
            vissit.readMore(items);
            vissit.deleteVisit(items, list);
        });

        const visitDentist = new VisitDentist();
        const visitCardiologist = new VisitCardiologist();
        const visitTherapist = new VisitTherapist();

        const selectDoctor = document.querySelector('.select-form');
        const selectOutput = document.querySelector('.select-output');

        selectDoctor.addEventListener('input', e => {
            selectOutput.textContent = '';
            if (e.target.value === 'Стоматолог') {
                selectOutput.insertAdjacentHTML('afterbegin', visitDentist.fildsDentist);
            } else if (e.target.value === 'Кардіолог') {
                selectOutput.insertAdjacentHTML('afterbegin', visitCardiologist.fildsCardiologist);
            } else if (e.target.value === 'Терапевт') {
                selectOutput.insertAdjacentHTML('afterbegin', visitTherapist.fildsTherapist);
            }
        });

        console.log('User Authorized');
    } else {
        list.displayList();
        buttonCreateVisit.classList.remove('show');
        buttonLogout.classList.remove('show');
        buttonLogin.classList.add('show');
        navFilter.classList.add('no-filter-content-hidden');
        noContent.classList.remove('no-content-hidden');
        console.log('User data empty');
    }
}

async function login(user) {
    try {
        const data = {
            email: user.email,
            password: user.password,
        };

        const response = await request({
            url: '/cards/login',
            method: 'POST',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' },
        });

        if (response.res) {
            user.setToken(response.res);
            const modals = document.querySelectorAll('.modal-login.active');
            modals.forEach(modal => {
                closeModal(modal);
            });

            checkUserAuth();
        } else {
            err.textContent = 'Введіть правильні значення';
        }
    } catch (error) {
        console.error(error.response.data);

        err.innerHTML = error.response.data;

        setTimeout(function () {
            err.innerHTML = '';
        }, 1500);
    }
}

checkUserAuth();

document.getElementById('filter-form').addEventListener('input', function (e) {
    e.preventDefault();
    const form = new FormData(this);
    const filters = {
        context: form.get('context'),
        priority: form.get('priority'),
    };

    list.displayFilteredList(filters).then(item => {
        vissit.deleteVisit(item, list);
    });
});
